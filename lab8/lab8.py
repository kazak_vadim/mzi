import skimage
import numpy as np
from skimage import io
from scipy.fftpack import dct, idct
from matplotlib import pyplot as plt


class FileManager:

    @classmethod
    def save(cls, data, path):
        with open(path, 'w+', encoding="utf-8") as file:
            file.write(data)

    @classmethod
    def read(cls, path):
        with open(path, 'r+') as file:
            data = file.read()
            return data

    @classmethod
    def get_image(cls, image_path):
        return io.imread(image_path)



class Converter:

    @classmethod
    def to_bin(cls, data):
        array = list()
        for char in data:
            binval = cls._binvalue(char, 8) 
            array.extend([int(x) for x in list(binval)])
        return array

    @classmethod
    def _binvalue(cls, data, bitsize):
        binval = bin(data)[2:] if isinstance(data, int) else bin(ord(data))[2:]
        while len(binval) < bitsize:
            binval = "0" + binval
        return binval

    @classmethod
    def double_to_byte(cls, data):
        return np.uint8(np.round(np.clip(data, 0, 255), 0))

    @classmethod
    def to_string(cls, data):
        chars = []
        for i in range(len(data) // 8):
            byte = data[i * 8:(i + 1) * 8]
            byte_str = ''.join(map(str, byte))
            chars.append(chr(int(byte_str, 2)))
        return ''.join(chars)


class Stegano:

    def __init__(self):
        self.u1 = 1
        self.v1 = 3
        self.u2 = 3
        self.v2 = 1
        self.n = 8
        self.tgreshold = 50

    def _increment_abs(self, x):
        if x >= 0:
            return x + 1
        else:
            return x - 1

    def _decrement_abs(self, x):
        if np.abs(x) <= 1:
            return 0
        elif x >= 0:
            return x - 1
        else:
            return x + 1

    def _get_bit_from_block(self, block):
        row_dct = dct(block, axis=0)
        patch = dct(row_dct, axis=1)
        result = abs(patch[self.u1, self.v1]) - abs(patch[self.u2, self.v2])
        if result > 0:
            return 0
        else:
            return 1

    def _valid_coeffs(self, block, bit, threshold):
        difference = abs(block[self.u1, self.v1]) - abs(block[self.u2, self.v2])
        if bit == 0 and difference > threshold:
            return True
        elif bit == 1 and difference < -threshold:
            return True
        else:
            return False

    def _change_coeffs(self, block, bit):
        coefs = block.copy()
        if bit == 0:
            coefs[self.u1, self.v1] = self._increment_abs(coefs[self.u1, self.v1])
            coefs[self.u2, self.v2] = self._decrement_abs(coefs[self.u2, self.v2])
        elif bit == 1:
            coefs[self.u1, self.v1] = self._decrement_abs(coefs[self.u1, self.v1])
            coefs[self.u2, self.v2] = self._increment_abs(coefs[self.u2, self.v2])
        return coefs

    def _hide_bit(self, block, bit):
        patch = block.copy()
        row_dct = dct(patch, axis=0)
        coefficients = dct(row_dct, axis=1)
        while not self._valid_coeffs(coefficients, bit, self.tgreshold) or (
                bit != self._get_bit_from_block(block)):
            coefficients = self._change_coeffs(coefficients, bit)
            inverse_row_dct = idct(coefficients, axis=0)
            block = Converter.double_to_byte(idct(inverse_row_dct, axis=1) / (2 * self.n) ** 2)
        return block

    def hide_message(self, text, image):
        bit_text = Converter.to_bin(text)
        image_duplicate = image.copy()
        blue_pixels = image_duplicate[:, :, 2]
        width, height = np.shape(blue_pixels)
        width -= width % self.n
        height -= height % self.n
        blue_pixels = blue_pixels[:width, :height]
        blocks = skimage.util.view_as_blocks(blue_pixels, block_shape=(self.n, self.n))
        h = blocks.shape[1]
        for index, bit in enumerate(bit_text):
            i, j = index // h, index % h
            block = blocks[i, j]
            blue_pixels[i * self.n: (i + 1) * self.n, j * self.n: (j + 1) * self.n] = self._hide_bit(block, bit)
        image_duplicate[:width, :height, 2] = blue_pixels
        return image_duplicate, len(bit_text)

    def get_hidden_text(self, image_path, text_len):
        image = io.imread(image_path)
        image = image[:, :, 2]
        width, height = np.shape(image)
        width -= width % self.n
        height -= height % self.n
        image = image[:width, :height]
        blocks = skimage.util.view_as_blocks(image, block_shape=(self.n, self.n))
        h = blocks.shape[1]
        text = []
        for i in range(text_len):
            text.append(self._get_bit_from_block(blocks[i // h, i % h]))
        result = Converter.to_string(text)
        return result



original_image_path = "s coupe.png"
changed_image_path = "result s coupe.png"
message_path = "text.txt"

message = FileManager.read(message_path)
original_image = FileManager.get_image(original_image_path)
stegano = Stegano()
print("Message: ", message)
changed_image, text_len = stegano.hide_message(message, original_image)
io.imsave(changed_image_path, changed_image)
result_message = stegano.get_hidden_text(image_path=changed_image_path, text_len=text_len)
print("Result message: ", result_message)
io.imshow(np.hstack((original_image, changed_image)))
plt.show()


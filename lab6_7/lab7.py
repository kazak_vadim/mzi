from lab6_7.utils import *
from lab6_7.consts import CurveParams


class DiffieHellman:

    @classmethod
    def get(self, d_a, d_b, G):
        curve = Curve(CurveParams.P, CurveParams.Q, CurveParams.A, CurveParams.B, CurveParams.X, CurveParams.Y)
        g_x, g_y = G
        key_A = curve.exp(d_a, g_x, g_y)
        key_B = curve.exp(d_b, g_x, g_y)
        secret_1 = curve.exp(d_a, *key_B)
        secret_2 = curve.exp(d_b, *key_A)
        print("Shared secret 1 {0} \nShared secret 2 {1}".format(*secret_1, *secret_2))
        result = secret_1 == secret_2
        return result


d_a, d_b = 123123, 432123
G = (123123, 12312311)
print("Shared secret ", DiffieHellman.get(d_a, d_b, G))

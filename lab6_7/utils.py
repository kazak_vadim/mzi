from codecs import getencoder


def egcd(a, b):
    if a == 0:
        return b, 0, 1
    else:
        g, y, x = egcd(b % a, a)
        return g, x - (b // a) * y, y


def modinvert(a, m):
    g, x, y = egcd(a, m)
    return x % m


def bytes_two_long(raw):
    return int(getencoder("hex")(raw)[0].decode("ascii"), 16)


def long_two_bytes(n, size):
    return n.to_bytes(size, "big")


class Curve(object):
    def __init__(self, p, q, a, b, x, y):
        self.p = bytes_two_long(p)
        self.q = bytes_two_long(q)
        self.a = bytes_two_long(a)
        self.b = bytes_two_long(b)
        self.x = bytes_two_long(x)
        self.y = bytes_two_long(y)

    def _pos(self, v):
        return v + self.p if v < 0 else v

    def add(self, p1x, p1y, p2x, p2y):
        if p1x == p2x and p1y == p2y:
            t = ((3 * p1x * p1x + self.a) * modinvert(2 * p1y, self.p)) % self.p
        else:
            tx = self._pos(p2x - p1x) % self.p
            ty = self._pos(p2y - p1y) % self.p
            t = (ty * modinvert(tx, self.p)) % self.p
        tx = self._pos(t * t - p1x - p2x) % self.p
        ty = self._pos(t * (p1x - tx) - p1y) % self.p
        return tx, ty

    def exp(self, degree, x=None, y=None):
        x = x or self.x
        y = y or self.y
        tx, ty = x, y
        degree -= 1
        while degree != 0:
            if degree & 1 == 1:
                tx, ty = self.add(tx, ty, x, y)
            degree = degree >> 1
            x, y = self.add(x, y, x, y)
        return tx, ty

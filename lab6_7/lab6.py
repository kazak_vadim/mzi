from os import urandom
from hashlib import md5
from lab6_7.utils import *
from lab6_7.consts import CurveParams


class FileManager:

    @classmethod
    def save(cls, data, path):
        with open(path, 'w+', encoding="utf-8") as file:
            file.write(data)

    @classmethod
    def read(cls, path):
        with open(path, 'r+') as file:
            data = file.read()
            return data


def generate_number(size):
    return bytes_two_long(urandom(size))


def sign(curve, private_key, h):
    a = bytes_two_long(h)
    e = a % curve.q
    if e == 0:
        e = 1
    while True:
        k = generate_number(64) % curve.q
        if k == 0:
            continue
        r = curve.exp(k)[0] % curve.q
        if r == 0:
            continue
        s = ((r * private_key) + (k * e)) % curve.q
        if s == 0:
            continue
        break
    result = long_two_bytes(s, 64) + long_two_bytes(r, 64)
    return result


def check(curve, public_key, h, signature):
    s, r = bytes_two_long(signature[:64]), bytes_two_long(signature[64:])
    if s <= 0 or s >= curve.q or r <= 0 or r >= curve.q:
        return False
    e = bytes_two_long(h) % curve.q
    if e == 0:
        e = 1
    v = modinvert(e, curve.q)
    z1, z2 = s * v % curve.q, curve.q - r * v % curve.q
    p1x, p1y = curve.exp(z1)
    q1x, q1y = curve.exp(z2, public_key[0], public_key[1])
    x_c, y_c = curve.add(p1x, p1y, q1x, q1y)
    result = x_c == r
    return result


text_path = "text.txt"

curve = Curve(CurveParams.P, CurveParams.Q, CurveParams.A, CurveParams.B, CurveParams.X, CurveParams.Y)
data = FileManager.read(text_path).encode("utf-8")
h = md5(data).digest()
private_key = generate_number(32)
public_key = curve.exp(private_key)

signature = sign(curve=curve, private_key=private_key, h=h)
result = check(curve=curve, public_key=public_key, signature=signature, h=h)
print("successfully") if result else print("failed")


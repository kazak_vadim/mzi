import math
import hmac
import hashlib


class Md5:

    _shift_counts = [7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
                     5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20,
                     4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
                     6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21]

    def new(self, msg):
        a_0, b_0, c_0, d_0 = [0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476]
        K = self._get_K()
        msg = self._prepare_message(msg)
        for chunk_ofst in range(0, len(msg), 64):
            a, b, c, d = a_0, b_0, c_0, d_0
            part = msg[chunk_ofst:chunk_ofst + 64]
            for i in range(64):
                f = self._F_func(b, c, d, i)
                g = self._G_func(i)
                M = int.from_bytes(part[4 * g:4 * g + 4], byteorder='little')
                to_rotate = a + f + K[i] + M
                b_ = b + self._left_rotate(to_rotate, self._shift_counts[i])
                a, b, c, d = d, b_, b, c

            a_0 = (a_0 + a) % (2 ** 32)
            b_0 = (b_0 + b) % (2 ** 32)
            c_0 = (c_0 + c) % (2 ** 32)
            d_0 = (d_0 + d) % (2 ** 32)

        result = int.to_bytes(a_0, 16, byteorder="little").rstrip(b'\0') + \
                 int.to_bytes(b_0, 16, byteorder="little").rstrip(b'\0') + \
                 int.to_bytes(c_0, 16, byteorder="little").rstrip(b'\0') + \
                 int.to_bytes(d_0, 16, byteorder="little").rstrip(b'\0')

        return int.from_bytes(result, byteorder="little")

    def _G_func(self, i):
        if i < 16:
            return i
        elif i < 32:
            return (5 * i + 1) % 16
        elif i < 48:
            return (3 * i + 5) % 16
        else:
            return (7 * i) % 16

    def _F_func(self, b, c, d, i):
        if i < 16:
            return (b & c) | (~b & d)
        elif i < 32:
            return (d & b) | (~d & c)
        elif i < 48:
            return b ^ c ^ d
        else:
            return c ^ (b | ~d)

    def _prepare_message(self, message):
        message = bytearray(message)
        bits_len = (8 * len(message))
        message.append(0x80)
        while len(message) % 64 != 56:
            message.append(0)
        message += bits_len.to_bytes(8, byteorder='little')
        return message

    def _get_K(self):
        K = []
        for i in range(64):
            K.append(int(abs(math.sin(i + 1)) * 2 ** 32) % (2 ** 32))
        return K

    def _left_rotate(self, x, count):
        x %= 2 ** 32
        return ((x << count) | (x >> (32 - count))) % 2 ** 32


class Hmac:

    def new(self, key, msg, digestmod):
        block_size = 64
        if len(key) > block_size:
            key = int.to_bytes(digestmod.new(key), block_size, byteorder="little").rstrip(b'\0')
        key = key.ljust(block_size, b'\0')
        ipad = bytes(b'\x36' * block_size)
        opad = bytes(b'\x5c' * block_size)
        ikeypad = self._xor(key, ipad)
        okeypad = self._xor(key, opad)
        ikey_msg_hash = digestmod.new(ikeypad + msg)
        inner_hash = int.to_bytes(ikey_msg_hash, block_size, byteorder="little").rstrip(b'\0')
        outer_hash = digestmod.new(okeypad + inner_hash)
        h_hex = self._to_hex(outer_hash)
        return h_hex

    def _xor(self, val1, val2):
        return bytes(a ^ b for (a, b) in zip(val1, val2))

    def _to_hex(self, hash):
        raw = hash.to_bytes(16, byteorder='little')
        return '{:032x}'.format(int.from_bytes(raw, byteorder='big'))


key = b"QWERTF&*&*&__))*&^%$$##XSK%^&GBCUHDJBnsjkIUH^&*(BBS:LSASD:LSD"
msg = b"Hello world!"

hmac_obj = Hmac()
print("hmac \t ", hmac_obj.new(key=key, msg=msg, digestmod=Md5()))
print("hmac lib:", hmac.new(key=key, msg=msg, digestmod=hashlib.md5).hexdigest())
